
import { getWalletsName, isUnlocked } from "./utils";

// export const getWalletsName = (wallets, wallet) => wallets.find(item => item.address === wallet.address).name;
const wallet = {address:'abc'};
const wallets = [{address:'abc', name: 'wallet1'},{address:'fgh', name: 'wallet1'},{address:'xyz', name: 'wallet1'}]

describe('isUnlocked', () => {
    it('should find boolean of unlocked wallet', () => {
        const expectedResult = true; 
        const actualResult = isUnlocked(wallet, wallets);
        expect(actualResult).toEqual(expectedResult);
    });

    it('should not find boolean of unlocked wallet', () => {
        const expectedResult = false; 
        const actualResult = isUnlocked({address:'wubbalubba'}, wallets);
        expect(actualResult).toEqual(expectedResult);
    })

    it('should return false when malformed data', () => {
        const expectedResult = false; 
        const actualResult = isUnlocked(wallets);
        expect(actualResult).toEqual(expectedResult);
    })
})

describe('getWalletsName', () => {
    it('should return the name', () => {
        const expectedResult = 'wallet1'; 
        const actualResult = getWalletsName(wallets, wallet);
        expect(actualResult).toEqual(expectedResult);
    })
    it('should return null when no name', () => {
        const expectedResult = undefined; 
        const actualResult = getWalletsName(wallets, {address: 'wubbalubba'});
        expect(actualResult).toEqual(expectedResult);
    })
    it('should handle missing data', () => {
        const expectedResult = undefined; 
        const actualResult = getWalletsName(wallets);
        expect(actualResult).toEqual(expectedResult);
    })

    it('should handle malformed wallet data', () => {
        const expectedResult = undefined; 
        const actualResult = getWalletsName(wallets, 'wubbalubba');
        expect(actualResult).toEqual(expectedResult);
    })
})