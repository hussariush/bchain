
export const isUnlocked = (wallet = {}, unlockedWallets = []) => !!unlockedWallets.find(item => item.address === wallet.address);

export const getWalletsName = (wallets, wallet) => wallets && wallet && wallets.find(item => item.address === wallet.address)?.name;