import React, { useState, useEffect } from 'react';
import './App.css';
import { ethers } from 'ethers';
import { isUnlocked, getWalletsName } from './Utils/utils'

function App() {
  const [walletName, setWalletName] = useState(null);
  const [wallets, setWallets] = useState([]);
  const [error, setError] = useState(null);
  const [unlockedBalances, setUnlockedBalances] = useState({});
  const [unlockedWallets, setUnlockedWallets] = useState([]);
  const [targetAddress, setTargetAddress] = useState('');
  const [sourceAddress, setSourceAddress] = useState('');
  const [amountToSend, setAmountToSend] = useState('');
  const [provider,] = useState(ethers.getDefaultProvider('rinkeby'));
  const [isLoading, setIsLoading] = useState(false);


  useEffect(() => {
    setWalletsLocally();
  }, [])


  useEffect(() => {
    localStorage.setItem('wallets', JSON.stringify(wallets));
  }, [wallets])

  useEffect(() => {
    const poll = () => {
      provider.on('block', async () => {
        // Emitted whenever a DAI token transfer occurs
        if (unlockedWallets.length) {
          const balances = await getBalances();
          setUnlockedBalances(balances);
        }
      })
    }
    poll();
    return () => {
      provider.removeAllListeners();
    }
  }, [provider, unlockedWallets])

  const setWalletsLocally = () => {
    const walletsArray = JSON.parse(localStorage.getItem('wallets'));
    if (walletsArray?.length) {
      setWallets(walletsArray);
    }
  }

  const onChangeHandler = (e) => setWalletName(e.target.value);

  const unlockWallet = async (json) => {
    const password = window.prompt('Password to unlock')
    setIsLoading(true);
    setError(null);
    let wallet = null;
    try {
      wallet = await ethers.Wallet.fromEncryptedJson(json, password);
      wallet = wallet.connect(provider);
      setUnlockedWallets([...unlockedWallets, wallet]);
    } catch (error) {
      setError(error.message);
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  }

  const createWalletHandler = (e) => {
    e.preventDefault();
    setIsLoading(true)
    setError(null);
    try {
      const wallet = ethers.Wallet.createRandom();
      const password = window.prompt('Password');
      wallet.connect(provider);
      password && wallet.encrypt(password).then(res => {
        const withWalletName = [...wallets, { json: res, name: walletName, address: wallet.address }]
        setWallets(withWalletName)
        setIsLoading(false)
      })
    } catch (error) {
      console.log(error);
      setError(error.message);
    } finally {
      setIsLoading(false);
    }
  }

  const getBalances = async () => {
    let balancesObject = null;
    try {
      const balanceRequests = unlockedWallets.map(item => item.getBalance());
      await Promise.all(balanceRequests).then(res => {
        const balancesArray = res.map((item, index) => {
          const result = { [unlockedWallets[index].address]: ethers.utils.formatEther(item) };
          return result
        })
        balancesObject = Object.assign({}, ...balancesArray);
      });
    } catch (error) {
      console.log(`'balance error'`, error)
    }
    return balancesObject;
  }

  const sendEthHandler = async (e) => {
    e.preventDefault();
    if (!sourceAddress || !targetAddress) {
      return false;
    }
    const petrolPrice = await provider.getGasPrice();
    const walletSigner = unlockedWallets.find(item => item.address === sourceAddress);

    const tx = {
      from: walletSigner.address,
      to: targetAddress,
      value: ethers.utils.parseEther(amountToSend),
      nonce: provider.getTransactionCount(walletSigner.address, 'latest'),
      gasLimit: ethers.utils.hexlify(100000), // 100000
      gasPrice: petrolPrice
    }

    try {
      const res = await walletSigner.sendTransaction(tx);
      console.log(`res`, res)
    } catch (error) {
      console.error(`error`, error.message, error.code)
    }
  }

  const targetWalletChangeHandler = (e) => {
    setTargetAddress(e.target.value)
  }

  const sourceWalletChangeHandler = (e) => {
    setSourceAddress(e.target.value)
  }

  const targetValueChangeHandler = (e) => {
    setAmountToSend(e.target.value)
  }

  return (
    <div className="App container">
      <h1>Your wallets</h1>
      <form onSubmit={createWalletHandler}>
        <legend>Create wallet</legend>
        <label htmlFor='wallet-name'>Wallet name</label>
        <input placeholder='Add wallet name' id='wallet-name' type='text' onInput={onChangeHandler} name='wallet-name' />
        <button disabled={!walletName} type='submit'>Create</button>
      </form>
      {isLoading && <p>Please wait...</p>}
      <section>
        <h2>Wallets</h2>
        <ul>
          {wallets.map((wallet, index) => (
            <li className='row' key={index}>
              <span className='column column-25'>Name: </span>
              <span className='column column-50'>{wallet.name} </span>
              {!isUnlocked(wallet, unlockedWallets) && <button onClick={() => unlockWallet(wallet.json)}>Unlock</button>}
            </li>
          ))}
        </ul>
      </section>
      <section>
        <h2>Unlocked wallets</h2>
        <ul>
          {unlockedWallets.map(wallet => (
            <li className='row' key={wallet.address}>
              <div className='column'>
                <span>Name: </span>
                <span>{getWalletsName(wallets, wallet)}</span>
              </div>
              <div className='column'>
                <span>Address: </span>
                <span>{wallet.address} </span>
              </div>
              <div className='column'>
                <span>Balance: </span>
                <span>{unlockedBalances?.[wallet.address] ?? 'n/a'}</span>
              </div>
            </li>
          ))}
        </ul>
      </section>
      <div>

        {unlockedWallets.length ? <form onSubmit={sendEthHandler}>
          <legend>Send tokens</legend>
          <div>
            <label htmlFor='source'>From </label>
            <select id='source' onChange={sourceWalletChangeHandler}>
              <option selected>Source</option>
              {unlockedWallets.map(wallet => (
                <option key={wallet.address} value={wallet.address}>{getWalletsName(wallets, wallet)}</option>
              ))}
            </select>
          </div>
          <div>
            <label htmlFor='target'>To</label>
            <select id='target' onChange={targetWalletChangeHandler}>
              <option selected>Target</option>
              {wallets.map(wallet => (
                <option key={wallet.address} value={wallet.address}>{wallet.name}</option>
              ))}
            </select>
          </div>
          <div>

            <label htmlFor='amount'>Amount</label>
            <input id='amount' placeholder='how much' type='text' inputMode='decimal' onChange={targetValueChangeHandler} />
          </div>

          <button type='submit'>Send</button>
        </form>
          : <p>Unlock a wallet to send tokens</p>}
        {error && <p>{error}</p>}
      </div>
    </div>
  );
}

export default App;
